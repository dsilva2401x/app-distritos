module.exports = function ($) {
	var r = {};

	r.getAll = function (req, res) {
		res.end('Get All');
	}

	r.getOne = function (req, res) {
		res.end('Get One');
	}

	return r;
}