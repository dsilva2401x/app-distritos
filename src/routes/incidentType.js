module.exports = function ($) {
	var r = {};
	var Person = $.database.main.models.Person;

	r.getAll = function ( req, res ) {
		res.end('Get all');
	}

	r.getOne = function ( req, res ) {
		res.end('Get one');
	}

	return r;
}