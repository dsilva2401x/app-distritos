module.exports = function ( $express, $app, $database ) {

	// Controllers dependencies
		var $ = {};
		$.database = $database;

	// Routes
		var viewsRouter = $express.Router();
		var authRouter = $express.Router();
		var apiRouter = $express.Router();

	// Controllers
		var auth = require('./auth')($);
		var views = require('./views')($);
		var person = require('./person')($);
		var incidentType = require('./incidentType')($);
		var district = require('./district')($);
		var entity = require('./entity')($);

	// Views
		viewsRouter.get('/login', views.login);
		viewsRouter.get('/register', views.register)

	// Auth
		authRouter.post('/login', auth.login);
		authRouter.post('/logout', auth.logout);

	// API
		// Person
		apiRouter.get('/me', person.meGet);
		apiRouter.put('/me', person.mePut);
		apiRouter.get('/person', person.getAll);
		apiRouter.get('/person/:personId', person.getOne);
		apiRouter.post('/person', person.post);
		apiRouter.put('/person/:personId', person.put);
		apiRouter.delete('/person/:personId', person.delete);
		// Incident Type
		apiRouter.get('/incidentType', incidentType.getAll);
		apiRouter.get('/incidentType/:incidentTypeId', incidentType.getOne);
		// District
		apiRouter.get('/district', district.getAll);
		apiRouter.get('/district/:districtId', district.getOne);
		// Entity
		apiRouter.get('/entity', entity.getAll);
		apiRouter.get('/entity/:entityId', entity.getOne);

	
	// Set routers
		$app.use( viewsRouter );
		$app.use( '/auth', authRouter );
		$app.use( '/api', apiRouter );

}