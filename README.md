# Express Wrapper seed

Setup
--------

```bash
git clone https://github.com/dsilva2401/express-wrapper-seed my-app
cd my-app
npm install
node app.js
```

Database
--------

Basic tables *(src/models/index.js)*

![Database](docs/db.jpg)